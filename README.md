# Будут исключены из отслеживания:
## директории .terraform
## файлы .tfstate и .tfstate с расширениями
## файл лога
## все файлы с расширением tfvars
## файлы override.tf, override.tf.json
## и все файлы, оканчивающиеся на эти паттерны:
 *_override.tf, *_override.tf.json
## а также файлы 
.terraformrc, terraform.rc

